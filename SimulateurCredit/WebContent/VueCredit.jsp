<%@page import="web.CreditModel"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%
     CreditModel mod=(CreditModel)request.getAttribute("mod");
    %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Simulateur Credit</title>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/css/style.css">
</head>
<body>
<div>
<form action="ControleurServlet" method="post">
<table>
<tr>
<td>Montant: </td><td><input type="text" name="montant" value="<%=mod.getMontant() %>"/></td><td>Dinars</td>
</tr>
<tr>
<td>Duree: </td><td><input type="text" name="duree" value="<%=mod.getDuree() %>"/></td><td>Mois</td>
</tr>
<tr>
<td>Taux: </td><td><input type="text" name="taux" value="<%=mod.getTaux() %>"/></td><td>%</td>
</tr>
<tr><td><input type="submit" value="Calculer"/></td></tr>
</table>
</form>
</div>
<div>
<table>
<tr>
<td>Mensualite: </td><td><%=mod.getMensualite() %></td><td>Dinars</td>
</tr>
</table>
</div>
</body>
</html>